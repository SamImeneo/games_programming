#pragma once
#include "State.h"
#include "Player.h"
#include "Location.h"
#include "Path.h"
#include "CommandReader.h"
#include "SetupAdventure.h"
#include "Blackboard.h"

#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

class Adventure :
	public State
{
public:
	Adventure();
	~Adventure();
	void draw();
	void handleInput(StateManager*);

private:
	Player lPlayer;
	SetupAdventure lSetupAdventure;

	bool firstRun = true;
	std::vector<Location> locationVector;
	std::vector<Path> pathVector;
	CommandReader lCommandReader;

	Blackboard* lBlackboard;

};

