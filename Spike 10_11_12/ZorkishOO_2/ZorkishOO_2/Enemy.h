#pragma once
#include "HealthComponent.h"
#include "Location.h"

class Enemy :
	public Item
{
public:
	Enemy();
	~Enemy();

	HealthComponent* getHealthComp();

	void setLocation(Location&);
	Location getLocation();


private:
	HealthComponent* lHealthComponant;
	Location lLocation;
};

