#include "Inventory.h"
#include <vector>


using namespace std;
Inventory::Inventory()
{
}


Inventory::~Inventory()
{
}

void Inventory::addItem(Item& item)
{
	lInventory.push_back(item);
	
}

void Inventory::removeItem(int i)
{
	if (i != 0)
	{
		lInventory.erase(lInventory.begin() + i);
	}
	else
	{
		lInventory.erase(lInventory.begin() + 0);
	}
}

void Inventory::removeItem(string s)
{
	for (int i = 0; i < lInventory.size(); i++)
	{
		if (lInventory[i].getItemName() == s)
		{
			lInventory[i].~Item();
			lInventory.erase(lInventory.begin() + i);
		}
	}
}

int Inventory::getInventorySize()
{
	return lInventory.size();
}

Item Inventory::getItem(int i)
{
	return lInventory[i];
}

std::vector<Item> Inventory::getInventory()
{
	return lInventory;
}




