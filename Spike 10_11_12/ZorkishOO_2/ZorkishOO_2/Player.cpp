#include "Player.h"
#include <iostream>

using namespace std;
Player::Player()
{
	lBag.setName("Leatherbag");
	lBag.setID("xx");
	lBag.setDescription("Its a leather bag, you hold everything else inside it");

	HealthComponent* playersHC = new HealthComponent(10, 10);
	SetHealthComp(playersHC);

	AttackComponant* playerAC = new AttackComponant(2);
	SetAttackComp(playerAC);
}


Player::~Player()
{
	deleteHealthComp();
}

void Player::draw()
{
	cout << "im here!" << endl;
}

void Player::addItem(Item& item)
{
	lBag.getInventory()->addItem(item);
}

void Player::removeItem(int i)
{
	lBag.getInventory()->removeItem(i);
}

int Player::getInventorySize()
{
	return lBag.getInventorySize();
}

Item Player::getItem(int i)
{
	return lBag.getInventory()->getItem(i);
}

Bag* Player::getBag()
{
	return &lBag;
}

void Player::setLocation(Location& l)
{
	lLocation = l;
}

Location Player::getLocation()
{
	return lLocation;
}



HealthComponent * Player::getHealthComp()
{
	return lHealthComponant;
}

void Player::SetHealthComp(HealthComponent* hpComp)
{
	deleteHealthComp();
	lHealthComponant = hpComp;
}

void Player::deleteHealthComp()
{
	if (lHealthComponant != nullptr)
	{
		delete lHealthComponant;
	}
	lHealthComponant = nullptr;
}

AttackComponant * Player::getAttackComp()
{
	return lAttackComponant;
}

void Player::SetAttackComp(AttackComponant* atkComp)
{
	deleteAttackComp();
	lAttackComponant = atkComp;
}

void Player::deleteAttackComp()
{
	if (lAttackComponant != nullptr)
	{
		delete lAttackComponant;
	}
	lAttackComponant = nullptr;
}


void Player::subscribeToBlackboard(Blackboard* newBlackboard)
{
	lBlackboard = newBlackboard;
}

void Player::unSubscribeFromBlackboard()
{
	lBlackboard = nullptr;
}

void Player::updateBlackboard()
{
	if (lBlackboard != nullptr)
	{
		if (lBlackboard->checkForMessage("player"))
		{
			Message action;
			action = lBlackboard->getMessage("player");
			performAction(action);
		}
	}


}

void Player::performAction(Message action)
{
	if (action.getAction() == "attack")
	{
		if (getHealthComp() != nullptr)
		{
			lHealthComponant->Damage(action.getValue());
		}
	}

}

Blackboard * Player::getBlackboard()
{
	if (lBlackboard != nullptr)
	{
		return lBlackboard;
	}
	else
	{
		cout << "player blackboard null" << endl;
		return nullptr;
	}
}
