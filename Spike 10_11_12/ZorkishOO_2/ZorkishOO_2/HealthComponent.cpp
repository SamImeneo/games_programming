#include "HealthComponent.h"

using namespace std;
HealthComponent::HealthComponent()
{
	
}

HealthComponent::HealthComponent(int maxHp)
{
	lMaxHp = maxHp;
}

HealthComponent::HealthComponent(int currentHp, int maxHp)
{
	lCurrentHp = currentHp;
	lMaxHp = maxHp;
}


HealthComponent::~HealthComponent()
{
}

int HealthComponent::getCurrenthealth()
{
	return lCurrentHp;
}

std::string HealthComponent::getHealth()
{
	return "HP: " +  to_string(lCurrentHp) + " / " + to_string(lMaxHp);
}

void HealthComponent::Damage(int dmg)
{
	lCurrentHp -= dmg;
	if (lCurrentHp < 0)
	{
		lCurrentHp = 0;
	}

}

void HealthComponent::Healing(int heal)
{
	lCurrentHp += heal;
	if (lCurrentHp > lMaxHp)
	{
		lCurrentHp = lMaxHp;
	}
}
