#include "Blackboard.h"
#include <iostream>

using namespace std;
Blackboard::Blackboard()
{
	cout << "Blackboard created" << endl;
}


Blackboard::~Blackboard()
{
}

void Blackboard::addMessage(Message* newMessage)
{
	messageBoard.push_back(newMessage);
}

bool Blackboard::checkForMessage(string possibleReceiver)
{
	for each(Message* m in messageBoard)
	{
		if (m->getReceiver() == possibleReceiver)
		{
			return true;
		}
		else if (m->getReceiver() == "ALL")
		{
			return true;
		}
	}
	return false;
}

// this method can only be called after check for message
Message Blackboard::getMessage(string receiver)
{
	for each (Message* m in messageBoard)
	{
		if (m->getReceiver() == receiver)
		{
			return *m;
		}
		else if (m->getReceiver() == "ALL")
		{
			return *m;
		}
	}
	

	// prior validation should prevent this method from being called without the for each loop returning something
	return Message();
}

void Blackboard::clearMessages()
{
	for each (Message* m in messageBoard)
	{
		delete m;
	}
	messageBoard.clear();
}
