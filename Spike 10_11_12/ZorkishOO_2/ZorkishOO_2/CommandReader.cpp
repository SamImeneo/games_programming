#include "CommandReader.h"
#include "Player.h"


using namespace std;
CommandReader::CommandReader()
{
}


CommandReader::~CommandReader()
{
}

void CommandReader::readCommand(string userInput, Player& lPlayer, vector<Location>& locationVector, vector<Path>& pathVector)
{
	vector<string> vUserInput;
	// if there was a user input
	if (!userInput.empty())
	{
		stringstream ss(userInput);
		string token;

		// split the user input by spaces
		while (getline(ss, token, ' '))
		{
			vUserInput.push_back(token);
		}
				
		// if the user input was 2 characters long
		if (vUserInput.size() == 2)
		{
			// if the player was trying to move
			if (moveCommand(vUserInput[0]))
			{
				lCommandDoer.checkForLocation(vUserInput[1], lPlayer, locationVector, pathVector);
			} // if the player wanted to look at something
			else if (lookCommand(vUserInput[0]))
			{
				if (vUserInput[1] == lPlayer.getLocation().getName())
				{
					lCommandDoer.lookLocation(vUserInput[1], lPlayer);
				}
				else
				{
					lCommandDoer.lookItem(vUserInput[1], lPlayer);
				}
			}
			else if (peelCommand(vUserInput[0]))
			{
				lCommandDoer.peel(vUserInput[1], lPlayer);
			}
			else if (attackCommand(vUserInput[0]))
			{
				lCommandDoer.attack(vUserInput[1], lPlayer);
			}
		}
		else if (vUserInput.size() == 3)
		{
			if (moveCommandThree(vUserInput[0], vUserInput[1]))
			{
				lCommandDoer.checkForLocation(vUserInput[2], lPlayer, locationVector, pathVector);
			}
			if (lookCommandThree(vUserInput[0], vUserInput[1]))
			{
				lCommandDoer.lookItem(vUserInput[2], lPlayer);
			}
		}
		else if (vUserInput.size() == 5)
		{
			if (lookCommandFive(vUserInput))
			{
				lCommandDoer.lookAtIn(vUserInput[2], vUserInput[4], lPlayer, locationVector, pathVector);
			}
		}
		else
		{
			cout << "You cannot do that" << endl;
		}

	}
}



bool CommandReader::moveCommand(string playerMove)
{
	// transform the string to upper case
	transform(playerMove.begin(), playerMove.end(), playerMove.begin(), ::toupper);

	vector<string> moveCommands{ "GO", "MOVE", "GOTO" };

	for each (string s in moveCommands)
	{
		if (playerMove == s)
		{
			return true;
		}
	}
	return false;
}




bool CommandReader::moveCommandThree(string playerMoveOne, string playerMoveTwo)
{
	transform(playerMoveOne.begin(), playerMoveOne.end(), playerMoveOne.begin(), ::toupper);
	transform(playerMoveTwo.begin(), playerMoveTwo.end(), playerMoveTwo.begin(), ::toupper);

	vector<string> moveCommands{ "GO", "MOVE" };

	for each (string s in moveCommands)
	{
		if (playerMoveOne == s)
		{
			if (playerMoveTwo == "TO")
			{
				return true;
			}
		}
	}
	return false;
}

bool CommandReader::lookCommand(string playerLook)
{
	transform(playerLook.begin(), playerLook.end(), playerLook.begin(), ::toupper);

	vector<string> lookCommands{ "LOOK", "EXAMINE", "LOOKAT" };

	for each (string s in lookCommands)
	{
		if (playerLook == s)
		{
			return true;
		}
	}
	return false;
}

bool CommandReader::lookCommandThree(string playerLookOne, string playerLookTwo)
{
	transform(playerLookOne.begin(), playerLookOne.end(), playerLookOne.begin(), ::toupper);
	transform(playerLookTwo.begin(), playerLookTwo.end(), playerLookTwo.begin(), ::toupper);

	vector<string> lookCommands{ "LOOK", "EXAMINE" };

	for each (string s in lookCommands)
	{
		if (playerLookOne == s)
		{
			if (playerLookTwo == "AT")
			{
				return true;
			}
		}
	}
	return false;
}

bool CommandReader::peelCommand(string peel)
{
	transform(peel.begin(), peel.end(), peel.begin(), ::toupper);

	vector<string> lookCommands{ "PEEL" };

	for each (string s in lookCommands)
	{
		if (peel == s)
		{
			return true;
		}
	}
	return false;
}

bool CommandReader::attackCommand(string attack)
{
	transform(attack.begin(), attack.end(), attack.begin(), ::toupper);

	vector<string> attackCommands{ "ATTACK", "HIT", "STRIKE" };

	for each (string s in attackCommands)
	{
		if (attack == s)
		{
			return true;
		}
	}
	return false;
}

bool CommandReader::lookCommandFive(vector<string> vUserInput)
{
	for (int i = 0; i < vUserInput.size(); i++)
	{
		transform(vUserInput[i].begin(), vUserInput[i].end(), vUserInput[i].begin(), ::toupper);
	}

	if (vUserInput[0] == "LOOK")
	{
		if (vUserInput[1] == "AT")
		{
			if (vUserInput[3] == "IN")
			{
				return true;
			}
		}
	}
	return false;
}

