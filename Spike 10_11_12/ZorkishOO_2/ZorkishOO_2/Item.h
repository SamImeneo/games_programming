#pragma once
#include <iostream>
#include "Blackboard.h"
#include "Message.h"
#include "HealthComponent.h"

class Item
{
public:
	Item();
	~Item();
	virtual void setName(std::string);
	std::string getItemName();
	void setID(std::string);
	std::string getID();
	void setDescription(std::string);
	std::string getDescription();
	
	void subscribeToBlackboard(Blackboard*);
	void unSubscribeFromBlackboard();
	void updateBlackboard();
	void performAction(Message);

	HealthComponent* getHealthComp();
	void SetHealthComp(HealthComponent*);
	void deleteHealthComp();

private:
	std::string lName;
	std::string itemID;
	std::string itemDescription;
	
	Blackboard* lBlackboard = nullptr;

	HealthComponent* lHealthComponant = nullptr;

};

