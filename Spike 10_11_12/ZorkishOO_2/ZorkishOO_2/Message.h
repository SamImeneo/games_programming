#pragma once
#include <string>

class Message
{
public:
	Message();
	Message(std::string, int, std::string);
	~Message();

	int getValue();
	std::string getReceiver();
	std::string getAction();

private:
	int lValue;
	std::string lReceiver;
	std::string lAction;
};

