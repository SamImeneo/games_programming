#pragma once
#include "Message.h"
#include <string>
#include <vector>

class Blackboard
{
public:
	Blackboard();
	~Blackboard();

	void addMessage(Message*);

	bool checkForMessage(std::string);
	Message getMessage(std::string);
	
	void clearMessages();

private:
	std::vector<Message*> messageBoard;
};

/*
user enters command
command read figures out what it is
command doer makes the message and sends...
...it to either blackboard or directly to the receiver

*/