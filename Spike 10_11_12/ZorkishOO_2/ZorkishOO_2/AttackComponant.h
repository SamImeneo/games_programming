#pragma once
#include "HealthComponent.h"

class AttackComponant
{
public:
	AttackComponant();
	AttackComponant(int);

	~AttackComponant();
	std::string GetDamage() const;
	void Attack(HealthComponent*);
	int GetDamageInt();


private:
	int lDmg;

};

