#include "Adventure.h"
#include "StateManager.h"


using namespace std;
Adventure::Adventure()
{
	lBlackboard = new Blackboard();
	// the blackboard now exists, it needs to be passed down to child classes so that it cna have messages sent and read from it


}


Adventure::~Adventure()
{
}

void Adventure::draw()
{
	//system("CLS");
	if (firstRun)
	{
		// read in the text file to create the locations and paths
		lSetupAdventure.Setup(locationVector, pathVector, lPlayer, lBlackboard);
		// put the player in location 0 (starting location)
		lPlayer.setLocation(locationVector[0]);
		lPlayer.subscribeToBlackboard(lBlackboard);

		firstRun = false;
	}
	else
	{
		// read the player the description of where they are
		cout << lPlayer.getLocation().getDescription() << endl;


		// all items in the world check the blackboard for a message
		for each (Location l in locationVector)
		{
			for each (Item i in l.getInventory().getInventory())
			{
				// if the item is subscribed/registerd to a blackboard it will receive messages from it here
				i.updateBlackboard();
			}
		}// all item sin the players bag check the blackboard forr a message
		for each (Item i in lPlayer.getBag()->getInventory()->getInventory())
		{
			i.updateBlackboard();
		}
		// remove any remaining messages
		lBlackboard->clearMessages();

	}

}

void Adventure::handleInput(StateManager *lStateManager)
{
	string userInput;
	cin.clear();
	getline(cin, userInput);

	lCommandReader.readCommand(userInput, lPlayer, locationVector, pathVector);
}