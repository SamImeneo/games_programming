#pragma once
#include "Inventory.h"
#include "Location.h"
#include "Bag.h"

#include "Blackboard.h"

#include "HealthComponent.h"
#include "AttackComponant.h"


class Player
{
public:
	Player();
	~Player();
	void draw();
	void addItem(Item&);
	void removeItem(int);
	int getInventorySize();
	Item getItem(int);

	Bag* getBag();
	
	void setLocation(Location&);
	Location getLocation();

	HealthComponent* getHealthComp();
	void SetHealthComp(HealthComponent*);
	void deleteHealthComp();

	AttackComponant* getAttackComp();
	void SetAttackComp(AttackComponant*);
	void deleteAttackComp();

	void subscribeToBlackboard(Blackboard*);
	void unSubscribeFromBlackboard();
	void updateBlackboard();
	void performAction(Message);

	Blackboard* getBlackboard();

private:
	Bag lBag;
	Location lLocation;
	HealthComponent* lHealthComponant;
	AttackComponant* lAttackComponant;

	Blackboard* lBlackboard = nullptr;
};

