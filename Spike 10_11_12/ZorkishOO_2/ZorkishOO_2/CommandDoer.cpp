#include "CommandDoer.h"


using namespace std;
CommandDoer::CommandDoer()
{
}


CommandDoer::~CommandDoer()
{
}

void CommandDoer::checkForLocation(string loc, Player& lPlayer, vector<Location>& locationVector, vector<Path>& pathVector)
{
	// loc = where the player wants to go
	system("CLS");
	// if the player wants to go to their current location
	if (loc == lPlayer.getLocation().getName())
	{
		// tell them they are already there
		cout << "You are already at the " << lPlayer.getLocation().getName() << endl;
	}
	else // otherwise
	{
		bool success;
		success = false;
		// for each location that exists
		for each (Location l in locationVector)
		{
			// check if the location exists
			if (loc == l.getName())
			{
				// if it does, for each path
				for each (Path p in pathVector)
				{
					// check if this path joins those two locations

					// points holds 2 strings, each on the name of a location at either end of that path
					vector<Location> points;
					points.push_back(p.getPoints()[0]);
					points.push_back(p.getPoints()[1]);

					// first point = the users location, and the second point = thei destination, there is a valid path
					if ((points[0].getName() == lPlayer.getLocation().getName()) && (points[1].getName() == loc))
					{
						system("CLS");
						for (int i = 0; i < locationVector.size(); i++)
						{
							if (locationVector[i].getName() == points[1].getName())
							{
								lPlayer.setLocation(locationVector[i]);
								system("CLS");
								success = true;
								break;
							}
						}
						break;
					}
					else if ((points[0].getName() == loc) && (points[1].getName() == lPlayer.getLocation().getName()))
					{
						system("CLS");
						for (int i = 0; i < locationVector.size(); i++)
						{
							if (locationVector[i].getName() == points[0].getName())
							{
								lPlayer.setLocation(locationVector[i]);
								system("CLS");
								success = true;
								break;
							}
						}
						break;
					}
				}

				if (success == false)
				{
					cout << "you attempt to move to " << loc << " but trip and fall" << endl;
					if (lPlayer.getHealthComp() != nullptr)
					{
						lPlayer.getHealthComp()->Damage(1);
					}
					else
					{
						cout << "but you are invincible!" << endl;
					}
				}
			}
		}
	}
}

void CommandDoer::lookItem(string item, Player& lPlayer)
{
	bool descFound = false;
	
	for each (Item i in lPlayer.getLocation().getInventory().getInventory())
	{
		if (item == i.getItemName())
		{
			cout << i.getDescription() << endl;
			descFound = true;

			if (i.getHealthComp() != nullptr)
			{
				cout << i.getItemName() << ": " << endl << i.getHealthComp()->getHealth() << endl;
			}

		}
	}
	if (item == lPlayer.getBag()->getItemName())
	{
		cout << lPlayer.getBag()->getDescription() << endl;
		descFound = true;
		cout << "The " << lPlayer.getBag()->getItemName() << " contains:" << endl;

		for each (Item i in lPlayer.getBag()->getInventory()->getInventory())
		{
			cout << i.getItemName() << endl;
		}

	}

	if (!descFound)
	{
		cout << "You cannot see " << item << endl;
	}
}

void CommandDoer::lookAtIn(string item, string location, Player& lPlayer, vector<Location>& locationVector, vector<Path>& pathVector)
{
	// check if the players location, is the location they are looking in
	if (lPlayer.getLocation().getName() == location)
	{
		for each (Item i in lPlayer.getLocation().getInventory().getInventory())
		{
			if (i.getItemName() == item)
			{
				cout << i.getDescription() << endl;

				if (i.getHealthComp() != nullptr)
				{
					cout << i.getItemName() << ": " << endl << i.getHealthComp()->getHealth() << endl;
				}

			}
		}
	}// check if the location is the players bag
	else if (location == lPlayer.getBag()->getItemName())
	{
		lookForItemInBag(item, lPlayer);
	}
	else
	{
		cout << "There is no " << item << " in " << location << endl;
	}
}

void CommandDoer::lookForItemInBag(string item, Player& lPlayer)
{
	for (int i = 0; i < lPlayer.getBag()->getInventory()->getInventorySize(); i++)
	{
		if (item == lPlayer.getBag()->getInventory()->getItem(i).getItemName())
		{
			cout << lPlayer.getBag()->getInventory()->getItem(i).getDescription() << endl;
		}
	}
}

void CommandDoer::lookLocation(string location, Player& lPlayer)
{
	lPlayer.getLocation().getDescription();
	for each (Item i in lPlayer.getLocation().getInventory().getInventory())
	{
		cout << location << " contains: " << endl;
		cout << i.getItemName() << endl;
	}
}

void CommandDoer::peel(string banana, Player& lPlayer)
{
	for each (Item i in lPlayer.getBag()->getInventory()->getInventory())
	{

		if (i.getItemName() == banana)
		{
			Banana realBanana;
			realBanana.peelBanana();

			realBanana.setID(i.getID());
			realBanana.setName("PeeledBanana");
			realBanana.setDescription("It's a peeled banana. What'd u expect?");

			lPlayer.getBag()->getInventory()->removeItem(i.getItemName());
			lPlayer.getBag()->getInventory()->addItem(realBanana);
			cout << "You peeled the banana... you monster" << endl;
		}
	}
}

void CommandDoer::attack(string target, Player& lPlayer)
{
	bool found;
	found = false;

	transform(target.begin(), target.end(), target.begin(), ::toupper);

	// makes a message sent directly to every target in the area
	if (target == "AREA")
	{
		attackLocationAnnouncement(lPlayer);
		found = true;
	} // makes one message to the blackboard, read by everyone
	else if (target == "ALL")
	{
		attackAllAnnouncement(lPlayer);
		found = true;
	} // makes a message sent to the blackboard, for a specific target
	else
	{
		for each (Item i in lPlayer.getLocation().getInventory().getInventory())
		{
			if (i.getItemName() == target)
			{
				// create a new message, target is the receiver, lPlayer is the sender, attack is the action
				Message* lMessage = new Message(target, lPlayer.getAttackComp()->GetDamageInt(), "attack");
				// add the message to the blackboard
				lPlayer.getBlackboard()->addMessage(lMessage);
				found = true;
			}
		}
	}
	if (found == false)
	{
		cout << "There is no " << target << " to attack" << endl;
	}

}

void CommandDoer::attackLocationAnnouncement(Player& lPlayer)
{
	bool anyItems;
	anyItems = false;

	
	// for each item in the players location
	for each (Item i in lPlayer.getLocation().getInventory().getInventory())
	{
		// create a new message, target is the receiver, lPlayer is the sender, attack is the action
		Message* lMessage = new Message("AREA", lPlayer.getAttackComp()->GetDamageInt(), "attack");
		// add the message tot he blackboard
		i.performAction(*lMessage);
		anyItems = true;
	}
	if (anyItems == false)
	{
		cout << "There is nothing to attack" << endl;
	}

}

void CommandDoer::attackAllAnnouncement(Player& lPlayer)
{
	// create a new message, target is the receiver, lPlayer is the sender, attack is the action
	Message* lMessage = new Message("ALL", (lPlayer.getAttackComp()->GetDamageInt() * 5), "attack");
	// add the message to the blackboard
	lPlayer.getBlackboard()->addMessage(lMessage);
}

// needs to send the player, instead of the damage int so the it know if "ALL applies to them"
// or the anouncement message can go directly to each o the relevant items


