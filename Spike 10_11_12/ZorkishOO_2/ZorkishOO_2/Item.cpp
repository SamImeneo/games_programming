#include "Item.h"


using namespace std;
Item::Item()
{
	
}

Item::~Item()
{
}

void Item::setName(string itemName)
{
	lName = itemName;
}

string Item::getItemName()
{
	return lName;

}

void Item::setID(string id)
{
	itemID = id;
}

std::string Item::getID()
{
	return itemID;
}

void Item::setDescription(string description)
{
	itemDescription = description;
}

std::string Item::getDescription()
{
	return itemDescription;
}

void Item::subscribeToBlackboard(Blackboard* newBlackboard)
{
	lBlackboard = newBlackboard;
}

void Item::unSubscribeFromBlackboard()
{
	lBlackboard = nullptr;
}

void Item::updateBlackboard()
{
	if (lBlackboard != nullptr)
	{
		if (lBlackboard->checkForMessage(lName))
		{
			Message action;
			action = lBlackboard->getMessage(lName);
			performAction(action);
		}
	}
}

void Item::performAction(Message action)
{
	if (action.getAction() == "attack")
	{
		if (getHealthComp() != nullptr)
		{
			lHealthComponant->Damage(action.getValue());
			cout << "You attack " << lName << endl;
			cout << lHealthComponant->getHealth() << endl;
		}
		else
		{
			cout << "You cannot attack " << lName << endl;
		}
	}
}

HealthComponent* Item::getHealthComp()
{
	if (lHealthComponant != nullptr)
	{
		return lHealthComponant;
	}
	else
	{
		return nullptr;
	}
}

void Item::SetHealthComp(HealthComponent* hpComp)
{
	deleteHealthComp();
	lHealthComponant = hpComp;
}

void Item::deleteHealthComp()
{
	if (lHealthComponant != nullptr)
	{
		delete lHealthComponant;
	}
	lHealthComponant = nullptr;
}