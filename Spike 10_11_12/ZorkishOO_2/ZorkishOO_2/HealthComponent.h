#pragma once
#include <vector>
#include <string>

class HealthComponent
{
private:
	int lMaxHp;
	int lCurrentHp;
public:
	HealthComponent();
	HealthComponent(int maxHp);
	HealthComponent(int currentHp, int maxHp);

	~HealthComponent();

	int getCurrenthealth();
	std::string getHealth();
	void Damage(int dmg);
	void Healing(int heal);

};

