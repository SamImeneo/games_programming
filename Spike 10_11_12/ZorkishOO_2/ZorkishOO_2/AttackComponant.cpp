#include "AttackComponant.h"


using namespace std;
AttackComponant::AttackComponant()
{
}

AttackComponant::AttackComponant(int dmg)
{
	lDmg = dmg;
}


AttackComponant::~AttackComponant()
{
}

std::string AttackComponant::GetDamage() const
{
	return to_string(lDmg) + " damage";
}

int AttackComponant::GetDamageInt()
{
	return lDmg;
}

void AttackComponant::Attack(HealthComponent* attackTarget)
{
	attackTarget->Damage(lDmg);
}
