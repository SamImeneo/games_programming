#pragma once
#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include "Location.h"
#include "Path.h"
#include "Player.h"
#include "Item.h"
#include "Bag.h"
#include "Banana.h"
#include "Enemy.h"
#include "Blackboard.h"

class SetupAdventure
{
public:
	SetupAdventure();
	~SetupAdventure();

	void Setup(std::vector<Location>&, std::vector<Path>&, Player&, Blackboard*);
	void createBag(std::string, std::string, std::string, std::string, Player&, std::vector<Location>&, Blackboard*);
	void createItem(std::string, std::string, std::string, std::string, Player&, std::vector<Location>&, Blackboard*);
	void createBanana(std::string, std::string, std::string, std::string, Player&, std::vector<Location>&, Blackboard*);
	void createEnemy(std::string, std::string, std::string, int, std::string, Player&, std::vector<Location>&, Blackboard*);

	void putInLocation(Item&, std::string, Player&, std::vector<Location>&);
};

