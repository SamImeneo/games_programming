#include "Message.h"


using namespace std;
Message::Message()
{
}

Message::Message(string _receiver, int _value, string _action)
{
	lReceiver = _receiver;
	lValue = _value;
	lAction = _action;
}


Message::~Message()
{
}

int Message::getValue()
{
	return lValue;
}

std::string Message::getReceiver()
{
	return lReceiver;
}

std::string Message::getAction()
{
	return lAction;
}
