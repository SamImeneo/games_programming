#pragma once
#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <algorithm>

#include "Location.h"
#include "Path.h"
#include "Player.h"
#include "CommandDoer.h"


class CommandReader
{
public:
	CommandReader();
	~CommandReader();

	void readCommand(std::string, Player&, std::vector<Location>&, std::vector<Path>&);
	bool moveCommand(std::string);
	bool moveCommandThree(std::string, std::string);
	bool lookCommandFive(std::vector<std::string>);
	bool lookCommand(std::string);
	bool lookCommandThree(std::string, std::string);
	bool peelCommand(std::string);
	bool attackCommand(std::string);
	
private:
	CommandDoer lCommandDoer;
};

