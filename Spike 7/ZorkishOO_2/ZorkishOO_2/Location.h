#pragma once
#include <string>

class Location
{
public:
	Location();
	~Location();
	void setID(std::string);
	std::string getID();
	void setName(std::string);
	std::string getName();
	void setDescription(std::string);
	std::string getDescription();
private:
	std::string id;
	std::string name;
	std::string description;
};

