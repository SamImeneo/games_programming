#include "HighScore.h"
#include <iostream>
#include <string>
#include "StateManager.h"

using namespace std;
HighScore::HighScore()
{
}


HighScore::~HighScore()
{
}

void HighScore::draw()
{
	system("CLS");
	cout << "HighScore:" << endl;
	cout << "Your Score was: " << "<unimpressive score here>" << endl;
	cout << "Enter your Name: " << endl;
	cout << endl << "To return to the Main Menu, enter <1>" << endl;
}

void HighScore::handleInput(StateManager *lStateManager)
{
	string userInput;
	cin >> userInput;

	if (userInput == "1")
	{
		lStateManager->switchState(userInput);
	}
}
