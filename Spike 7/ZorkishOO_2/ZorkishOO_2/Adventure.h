#pragma once
#include "State.h"
#include "Player.h"
#include "Location.h"
#include "Path.h"
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

class Adventure :
	public State
{
public:
	Adventure();
	~Adventure();
	void draw();
	void handleInput(StateManager*);
	void setupAdventure();
	void checkForLocation(std::string);

	std::vector<Location> getLocVector();
	std::vector<Path> getPathVector();

	Player getPlayer();

private:
	Player lPlayer;

	bool firstRun = true;
	std::vector<Location> locationVector;
	std::vector<Path> pathVector;
};

