#pragma once
#include "Location.h"

#include <string>
#include <vector>

class Path
{
public:
	Path();
	~Path();
	void setID(std::string);
	std::string getID();
	void setPoints(std::vector<Location>);
	std::vector<Location> getPoints();
private:
	std::string id;
	Location point1;
	Location point2;
};

