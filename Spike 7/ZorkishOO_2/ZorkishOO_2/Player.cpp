#include "Player.h"
#include <iostream>

using namespace std;
Player::Player()
{
}


Player::~Player()
{
}

void Player::draw()
{
	cout << "im here!" << endl;
}

void Player::addItem(Item item)
{
	lInventory.addItem(item);
}

void Player::removeItem(int i)
{
	lInventory.removeItem(i);
}

int Player::getInventorySize()
{
	return lInventory.getInventorySize();
}

Item Player::getItem(int i)
{
	return lInventory.getItem(i);
}

void Player::setLocation(Location l)
{
	lLocation = l;
}

Location Player::getLocation()
{
	return lLocation;
}
