#include "Adventure.h"
#include "StateManager.h"

using namespace std;
Adventure::Adventure()
{
	
}


Adventure::~Adventure()
{
}

void Adventure::draw()
{
	//system("CLS");
	if (firstRun)
	{
		// read in the text file to create the locations and paths
		setupAdventure();
		// put the player in location 0 (starting location)
		lPlayer.setLocation(locationVector[0]);
		firstRun = false;
	}
	else
	{
		// read the player the description of where they are
		cout << lPlayer.getLocation().getDescription() << endl;
	}	
}

void Adventure::handleInput(StateManager *lStateManager)
{
	string userInput;
	cin.clear();
	getline(cin, userInput);
	vector<string> vUserInput;

	// if there was a user input
	if (!userInput.empty())
	{
		stringstream ss(userInput);
		string token;

		// split the user input by spaces
		while (getline(ss, token, ' '))
		{
			vUserInput.push_back(token);
		}
		if (vUserInput[0] == "go")
		{
			checkForLocation(vUserInput[1]);
		}
	}
}

void Adventure::setupAdventure()
{
	// read in text file, create all the locations etc here
	ifstream inFile("AdventureMain.txt");
	string line;

	

	if (inFile.is_open())
	{
		// while there is still text in the file, read it
		while (!inFile.eof())
		{
			// get a line
			getline(inFile, line);
			// if the line says "location"
			if (line == "Location")
			{
				//set the next line to be the id
				string locID;
				getline(inFile, locID);
				cout << "ID: " << locID << endl;
				
				// the one after to be the name
				string locName;
				getline(inFile, locName);
				cout << "Name: " << locName << endl;

				// and the next to be the desctiption
				string locDesc;
				getline(inFile, locDesc);
				cout << "Desc: " << locDesc << endl;

				// make a new location, and assign those values to the location
				Location lLocation;
				lLocation.setID(locID);
				lLocation.setName(locName);
				lLocation.setDescription(locDesc);

				// put that location in the vector of locations
				locationVector.push_back(lLocation);
				cout << "location vector size: " << locationVector.size() << endl;
			}

			// if the line says "path"
			if (line == "Path")
			{
				// set the next line to be the id
				string pathID;
				getline(inFile, pathID);
				cout << "path id: " << pathID << endl;
				
				// the line after that to be the first point of the path
				string point1;
				getline(inFile, point1);
				cout << "point 1: " << point1 << endl;

				// and the line after that to be the second point of the path
				string point2;
				getline(inFile, point2);
				cout << "point 2: " << point2 << endl;

				// create two locations
				Location p1;
				Location p2;

				// find the locations in the vector that match those locations, and assign them to the new lcoations
				for (int i = 0; i < locationVector.size(); i++)
				{
					if (locationVector[i].getName() == point1)
					{
						p1 = locationVector[i];
					}
					else if (locationVector[i].getName() == point2)
					{
						p2 = locationVector[i];
					}
				}

				// put those locations into a vector
				vector<Location> tempLocVector;
				tempLocVector.push_back(p1);
				tempLocVector.push_back(p2);
				// make a new path with those values
				Path lPath;
				lPath.setID(pathID);
				lPath.setPoints(tempLocVector);

				// put that path into a vector of paths
				pathVector.push_back(lPath);

			}
		}
		inFile.close();
	}

	for each (Location l in locationVector)
	{
		cout << "location: " << l.getName() << endl;
	}

	for each (Path p in pathVector)
	{
		cout << "path from: " << p.getPoints()[0].getName() << " to " << p.getPoints()[1].getName() << endl;
	}

}

void Adventure::checkForLocation(string loc)
{
	// loc = where the player wants to go

	// if the player wants to go to their current location
	if (loc == lPlayer.getLocation().getName())
	{
		// tell them they are already there
		cout << "You are already at the " << lPlayer.getLocation().getName() << endl;
	}
	else // otherwise
	{
		// for each location that exists
		for each (Location l in locationVector)
		{
			// check if the location exists
			if (loc == l.getName())
			{
				// if it does, for each path
				for each (Path p in pathVector)
				{
					// check if this path joins those two locations

					// points holds 2 strings, each on the name of a location at either end of that path
					vector<Location> points;
					points.push_back(p.getPoints()[0]);
					points.push_back(p.getPoints()[1]);

					// first point = the users location, and the second point = thei destination, there is a valid path
					if ((points[0].getName() == lPlayer.getLocation().getName()) && (points[1].getName() == loc))
					{
						cout << "there is a valid path" << endl;
						lPlayer.setLocation(points[1]);
						break;
					}
					else if ((points[0].getName() == loc) && (points[1].getName() == lPlayer.getLocation().getName()))
					{
						cout << "this is second valid path" << endl;
						lPlayer.setLocation(points[0]);
						break;
					}
					
					cout << points[0].getName() << " " << points[1].getName() << endl;
				}
			}
		}
		system("CLS");
		cout << "You cannot go that way" << endl;
	}
}

std::vector<Location> Adventure::getLocVector()
{
	return locationVector;
}

std::vector<Path> Adventure::getPathVector()
{
	return pathVector;
}

Player Adventure::getPlayer()
{
	return lPlayer;
}
