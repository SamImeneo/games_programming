#include "stdafx.h"
#include "CppUnitTest.h"

#include "../ZorkishOO_2/Adventure.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(CreateAdventure)
		{
			Adventure a;

			Assert::IsTrue(a.getLocVector().size() == 0);
		}

		TEST_METHOD(SetupLocations)
		{
			Adventure* a = new Adventure();

			std::vector<Location> locVec = a->getLocVector();

			Location l1;
			l1.setName("Beach");
			l1.setDescription("its a beach");
			locVec.push_back(l1);

			Location l2;
			l1.setName("Rockface");
			l1.setDescription("its a beach");
			locVec.push_back(l1);

			Location l3;
			l1.setName("Jungle");
			l1.setDescription("its a beach");
			locVec.push_back(l1);

			Location l4;
			l1.setName("Clearing");
			l1.setDescription("its a beach");
			locVec.push_back(l1);
			
			Location l5;
			l1.setName("Temple");
			l1.setDescription("its a beach");
			locVec.push_back(l1);

			Assert::IsFalse(locVec.size() == 0);
			Assert::IsTrue(locVec.size() == 5);
			

		}

		TEST_METHOD(SetupLocationsWithPath)
		{
			Adventure* a = new Adventure();

			std::vector<Location> locVec = a->getLocVector();

			Location l1;
			l1.setName("Beach");
			l1.setDescription("its a beach");
			locVec.push_back(l1);

			Location l2;
			l1.setName("Rockface");
			l1.setDescription("its a beach");
			locVec.push_back(l1);

			Path p1;

			p1.setPoints(std::vector<Location>{ l1, l2});

			std::vector<Location> lv = p1.getPoints();

			Assert::IsTrue(lv[0].getName() == l1.getName());
			Assert::IsTrue(lv[1].getName() == l2.getName());

		}

		TEST_METHOD(PlayerMove)
		{
			Adventure* a = new Adventure();
			std::vector<Location> locVec = a->getLocVector();

			Location l1;
			l1.setName("Beach");
			l1.setDescription("its a beach");
			locVec.push_back(l1);

			Location l2;
			l1.setName("Rockface");
			l1.setDescription("its a beach");
			locVec.push_back(l1);

			Path p1;
			p1.setPoints(std::vector<Location>{ l1, l2});
			std::vector<Location> lv = p1.getPoints();

			
			Player p = a->getPlayer();
			p.setLocation(locVec[0]);

			Assert::IsTrue(p.getLocation().getName() == "Beach");
			

			//Assert::IsTrue(p.getLocation().getName() == "Rockface");
		}


	};
}