#pragma once
#include "State.h"
class About :
	public State
{
public:
	About();
	~About();
	void draw();
	void handleInput(StateManager*);
};

