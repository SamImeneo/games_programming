#pragma once
#include <string>
#include "Inventory.h"

class Location
{
public:
	Location();
	~Location();
	void setID(std::string);
	std::string getID();
	void setName(std::string);
	std::string getName();
	void setDescription(std::string);
	std::string getDescription();

	Inventory getInventory();

	void addItem(Item&);
	void removeItem(int);
	int getInventorySize();
	Item getItem(int);

private:
	std::string id;
	std::string name;
	std::string description;
	Inventory lInventory;

};

