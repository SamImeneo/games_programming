#pragma once
#include "State.h"
#include "Player.h"
#include "Location.h"
#include "Path.h"
#include "CommandReader.h"
#include "SetupAdventure.h"

#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

class Adventure :
	public State
{
public:
	Adventure();
	~Adventure();
	void draw();
	void handleInput(StateManager*);
	void checkForLocation(std::string);

	std::vector<Location> getLocationVector();
	std::vector<Path> getPathVector();
	Player getPlayer();

private:
	Player lPlayer;
	SetupAdventure lSetupAdventure;

	bool firstRun = true;
	std::vector<Location> locationVector;
	std::vector<Path> pathVector;
	CommandReader lCommandReader;
};

