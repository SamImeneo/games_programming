#pragma once
#include "State.h"
class Help :
	public State
{
public:
	Help();
	~Help();
	void draw();
	void handleInput(StateManager*);
};

