#pragma once
#include "State.h"
class HighScore :
	public State
{
public:
	HighScore();
	~HighScore();
	void draw();
	void handleInput(StateManager*);
};

