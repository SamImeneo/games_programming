#include "About.h"
#include <iostream>
#include <string>
#include "StateManager.h"

using namespace std;
About::About()
{
}


About::~About()
{
}

void About::draw()
{
	cout << "About:" << endl;
	cout << "Created by: Sam Imeneo" << endl;
	cout << "Student No: 9511954" << endl;
	cout << endl << "To return to the Main Menu, enter <1>" << endl;
}

void About::handleInput(StateManager *lStateManager)
{
	string userInput;
	cin >> userInput;

	if (userInput == "1")
	{
		lStateManager->switchState(userInput);
	}
}
