#include "stdafx.h"
#include "CppUnitTest.h"
#include "../ZorkishOO_2/StateManager.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest
{		
	TEST_CLASS(StateManagerTester)
	{
	public:
		
		TEST_METHOD(HasCurrentState)
		{
			StateManager* sm = new StateManager;

			State* s = sm->getCurrentState();
			
			Assert::IsTrue(s == sm->getCurrentState());
		}

		TEST_METHOD(ChangeStateTest)
		{
			StateManager* sm = new StateManager;

			State* s = sm->getCurrentState();

			sm->switchState("2");
			Assert::IsFalse(s == sm->getCurrentState());

			State* s2 = sm->getCurrentState();

			Assert::IsFalse(s == s2);

			sm->switchState("3");

			State* s3 = sm->getCurrentState();

			Assert::IsTrue(s != s2);
			Assert::IsTrue(s2 != s3);
			Assert::IsTrue(s != s3);
		}

		TEST_METHOD(IncorectInputForChangeState)
		{
			StateManager* sm = new StateManager;

			State* s = sm->getCurrentState();
			
			sm->switchState("not a state");

			Assert::IsTrue(s == sm->getCurrentState());
		}

	};
}