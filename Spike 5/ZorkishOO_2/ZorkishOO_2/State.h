#pragma once
class StateManager;

class State
{
public:
	State();
	~State();
	virtual void draw() = 0;
	virtual void handleInput(StateManager*) = 0;
};

