#include "Gameplay.h"
#include <iostream>
#include <string>
#include "StateManager.h"

using namespace std;
Gameplay::Gameplay()
{
}


Gameplay::~Gameplay()
{
}

void Gameplay::draw()
{
	cout << "Gameplay:" << endl;
	cout << endl << "To return to the Main Menu, enter <QUIT>" << endl;
	cout << endl << "To view the High Scores, enter <HIGHSCORE>" << endl;
}

void Gameplay::handleInput(StateManager *lStateManager)
{
	string userInput;
	cin >> userInput;

	if (userInput == "QUIT")
	{
		lStateManager->switchState(userInput);
	}
	if (userInput == "HIGHSCORE")
	{
		lStateManager->switchState(userInput);
	}
}
