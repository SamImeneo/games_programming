#pragma once
#include "State.h"
#include "MainMenu.h"
#include "About.h"
#include "Help.h"
#include "HallOFame.h"
#include "SelectAdventure.h"
#include "Gameplay.h"
#include "HighScore.h"
#include <string>

class StateManager
{
public:
	StateManager();
	~StateManager();
	void draw();
	void handleInput();
	void switchState(std::string);
	State* getCurrentState();

private:
	State* currentState;
	MainMenu lMainMenu;
	About lAbout;
	Help lHelp;
	HallOFame lHallOFame;
	SelectAdventure lSelectAdventure;
	Gameplay lGameplay;
	HighScore lHighscore;
};

