#include "HallOFame.h"
#include <iostream>
#include <string>
#include "StateManager.h"

using namespace std;
HallOFame::HallOFame()
{
}


HallOFame::~HallOFame()
{
}

void HallOFame::draw()
{
	cout << "Hall O' Fame: " << endl;
	cout << "1. asd" << endl;
	cout << "2. qwe" << endl;
	cout << "3. zxc" << endl;
	cout << endl << "To return to the Main Menu, enter <1>" << endl;
}

void HallOFame::handleInput(StateManager *lStateManager)
{
	string userInput;
	cin >> userInput;

	if (userInput == "1")
	{
		lStateManager->switchState(userInput);
	}
}
