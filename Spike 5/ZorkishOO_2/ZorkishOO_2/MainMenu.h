#pragma once
#include "State.h"
class MainMenu :
	public State
{
public:
	MainMenu();
	~MainMenu();
	void draw();
	void handleInput(StateManager*);
};

