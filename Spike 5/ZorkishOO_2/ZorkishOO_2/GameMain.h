#pragma once
#include "StateManager.h"

#include <string>


class GameMain
{
	//fields here
	std::string gameState;

public:
	GameMain();
	~GameMain();
	void gameLoop();

	bool gameRunning;


private:
	StateManager lStateManager;
	
};

