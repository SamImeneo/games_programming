#include "MainMenu.h"
#include <iostream>
#include <string>
#include "StateManager.h"

using namespace std;
MainMenu::MainMenu()
{
}


MainMenu::~MainMenu()
{
}

void MainMenu::draw()
{
	cout << "Main Menu:" << endl;
	cout << "To go to About, enter <2>" << endl;
	cout << "To go to Help, enter <3>" << endl;
	cout << "To Select an Adventure, enter <4>" << endl;
	cout << "To view the Hall O' Fame, enter <5>" << endl;
}

void MainMenu::handleInput(StateManager* lStateManager)
{
	string userInput;
	cin >> userInput;

	if (userInput == "2")
	{
		lStateManager->switchState(userInput);
	}
	if (userInput == "3")
	{
		lStateManager->switchState(userInput);
	}
	if (userInput == "4")
	{
		lStateManager->switchState(userInput);
	}
	if (userInput == "5")
	{
		lStateManager->switchState(userInput);
	}
}
