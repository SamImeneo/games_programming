#include "StateManager.h"

using namespace std;
StateManager::StateManager()
{
	currentState = &lMainMenu;
}


StateManager::~StateManager()
{
}

void StateManager::draw()
{
	currentState->draw();
}

void StateManager::handleInput()
{
	currentState->handleInput(this);
}

void StateManager::switchState(string userInput)
{
	if (userInput == "1" || userInput == "QUIT")
	{
		currentState = &lMainMenu;
	}else if (userInput == "2")
	{
		currentState = &lAbout;
	}
	else if (userInput == "3")
	{
		currentState = &lHelp;
	}
	else if (userInput == "4")
	{
		currentState = &lSelectAdventure;
	}
	else if (userInput == "5")
	{
		currentState = &lHallOFame;
	}
	else if (userInput == "TEST")
	{
		currentState = &lGameplay;
	}
	else if (userInput == "HIGHSCORE")
	{
		currentState = &lHighscore;
	}

}

State * StateManager::getCurrentState()
{
	return currentState;
}
