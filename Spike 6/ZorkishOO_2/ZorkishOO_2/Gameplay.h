#pragma once
#include "State.h"
#include "Player.h"
#include "Item.h"
#include <vector>
#include <string>
#include <iostream>
#include <sstream>

class Gameplay :
	public State
{
public:
	Gameplay();
	~Gameplay();
	void draw();
	void handleInput(StateManager*);
private:
	Player lPlayer;
};

