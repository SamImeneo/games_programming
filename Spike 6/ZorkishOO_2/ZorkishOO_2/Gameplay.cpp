#include "Gameplay.h"
#include "StateManager.h"

using namespace std;
Gameplay::Gameplay()
{

}


Gameplay::~Gameplay()
{
}

void Gameplay::draw()
{
	system("CLS");
	cout << "Gameplay:" << endl;
	cout << endl << "To return to the Main Menu, enter <1>" << endl;
	cout << "To enter a High Score, enter <6>" << endl;
	cout << "To add and item to inventory, enter ADD<itemname>" << endl;
	cout << "To remove an item, enter REM<itemnumber>" << endl;
	cout << endl << "Inventory size: " << lPlayer.getInventorySize() << endl;
	for (int i = 0; i < lPlayer.getInventorySize(); i++)
	{
		cout << "Item no." << i << ": " <<  lPlayer.getItem(i).getItemName() << endl;
	}


}

void Gameplay::handleInput(StateManager *lStateManager)
{
	string userInput;
	cin.clear();
	getline(cin, userInput);
	vector<string> vUserInput;

	// if there was a user input
	if (!userInput.empty())
	{
		stringstream ss(userInput);
		string token;

		// split the user input by spaces
		while (getline(ss, token, ' '))
		{
			vUserInput.push_back(token);
		}

		if (vUserInput[0] == "ADD")
		{
			if (vUserInput.size() > 1)
			{
				Item item;
				string tempName;
				for (int i = 1; i < vUserInput.size(); i++)
				{
					tempName += vUserInput[i] + " ";
				}
				item.setName(tempName);
				lPlayer.addItem(item);
			}
		}
		else if(vUserInput[0] == "REM")
		{
			if (vUserInput.size() > 1)
			{
				int size = lPlayer.getInventorySize();
				if (size > 0 && size > stoi(vUserInput[1]))
				{
					lPlayer.removeItem(stoi(vUserInput[1]));
				}
			}
		}
	}
}
