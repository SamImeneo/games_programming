#include "stdafx.h"
#include "CppUnitTest.h"

#include "../ZorkishOO_2/Player.h"
#include "../ZorkishOO_2/Item.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest
{		
	TEST_CLASS(PlayerTests)
	{
	public:
		
		TEST_METHOD(AddToInventory)
		{
			Player* p = new Player();
			Assert::IsTrue(p->getInventorySize() == 0);

			Item i;
			i.setName("testItem");
			p->addItem(i);
			Assert::IsTrue(p->getInventorySize() == 1);

			Assert::IsTrue(p->getItem(0).getItemName() == i.getItemName());

			Item j;
			Item k;

			p->addItem(j);
			p->addItem(k);
			Assert::IsTrue(p->getInventorySize() == 3);
		}

		TEST_METHOD(RemoveFromInventory)
		{
			Player* p = new Player();
			Assert::IsTrue(p->getInventorySize() == 0);

			Item i;
			i.setName("testItem");
			p->addItem(i);

			p->removeItem(0);
			Assert::IsTrue(p->getInventorySize() == 0);
		}

	};
}