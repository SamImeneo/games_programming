#include "Adventure.h"
#include "StateManager.h"


using namespace std;
Adventure::Adventure()
{
}


Adventure::~Adventure()
{
}

void Adventure::draw()
{
	//system("CLS");
	if (firstRun)
	{
		// read in the text file to create the locations and paths
		lSetupAdventure.Setup(locationVector, pathVector, lPlayer);
		// put the player in location 0 (starting location)
		lPlayer.setLocation(locationVector[0]);
		firstRun = false;
	}
	else
	{
		// read the player the description of where they are
		cout << lPlayer.getLocation().getDescription() << endl;
	}

}

void Adventure::handleInput(StateManager *lStateManager)
{
	string userInput;
	cin.clear();
	getline(cin, userInput);

	lCommandReader.readCommand(userInput, lPlayer, locationVector, pathVector);
}

void Adventure::checkForLocation(string loc)
{
	// loc = where the player wants to go
	system("CLS");
	// if the player wants to go to their current location
	if (loc == lPlayer.getLocation().getName())
	{
		// tell them they are already there
		cout << "You are already at the " << lPlayer.getLocation().getName() << endl;
	}
	else // otherwise
	{
		// for each location that exists
		for each (Location l in locationVector)
		{
			// check if the location exists
			if (loc == l.getName())
			{
				// if it does, for each path
				for each (Path p in pathVector)
				{
					// check if this path joins those two locations

					// points holds 2 strings, each on the name of a location at either end of that path
					vector<Location> points;
					points.push_back(p.getPoints()[0]);
					points.push_back(p.getPoints()[1]);

					// first point = the users location, and the second point = thei destination, there is a valid path
					if ((points[0].getName() == lPlayer.getLocation().getName()) && (points[1].getName() == loc))
					{
						cout << "there is a valid path" << endl;
						lPlayer.setLocation(points[1]);
						break;
					}
					else if ((points[0].getName() == loc) && (points[1].getName() == lPlayer.getLocation().getName()))
					{
						cout << "this is second valid path" << endl;
						lPlayer.setLocation(points[0]);
						break;
					}
					system("CLS");
					
					cout << "you cannot go that way" << endl;
					
				}
			}
		}
	}
}
