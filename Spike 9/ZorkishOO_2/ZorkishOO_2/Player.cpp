#include "Player.h"
#include <iostream>

using namespace std;
Player::Player()
{
	lBag.setName("Leatherbag");
	lBag.setID("xx");
	lBag.setDescription("Its a leather bag, you hold everything else inside it");
}


Player::~Player()
{
}

void Player::draw()
{
	cout << "im here!" << endl;
}

void Player::addItem(Item& item)
{
	lBag.getInventory()->addItem(item);
}

void Player::removeItem(int i)
{
	lBag.getInventory()->removeItem(i);
}

int Player::getInventorySize()
{
	return lBag.getInventorySize();
}

Item Player::getItem(int i)
{
	return lBag.getInventory()->getItem(i);
}

Bag* Player::getBag()
{
	return &lBag;
}

void Player::setLocation(Location& l)
{
	lLocation = l;
}

Location Player::getLocation()
{
	return lLocation;
}
