#include "Location.h"


using namespace std;
Location::Location()
{
}


Location::~Location()
{
}

void Location::setID(string newID)
{
	id = newID;
}

std::string Location::getID()
{
	return id;
}

void Location::setName(string newName)
{
	name = newName;
}

std::string Location::getName()
{
	return name;
}

void Location::setDescription(string newDescription)
{
	description = newDescription;
}

std::string Location::getDescription()
{
	return description;
}

Inventory Location::getInventory()
{
	return lInventory;
}

void Location::addItem(Item& item)
{
	lInventory.addItem(item);
}

void Location::removeItem(int i)
{
	lInventory.removeItem(i);
}

int Location::getInventorySize()
{
	return lInventory.getInventorySize();
}

Item Location::getItem(int i)
{
	return lInventory.getItem(i);
}