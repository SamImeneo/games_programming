Location
Loc0
Beach
You are on a Beach. You can go into the Jungle or go up the beach towards a Rockface.

Location
Loc1
Rockface
You are at the Rockface, it's too steep to climb. You can go to the Beach.

Location
Loc2
Jungle
You are in the Jungle. You can go to the Beach, to a Clearing or to the Temple.

Location
Loc3
Clearing
You are in a clearing. You can go into the Jungle or to the Temple.

Location
Loc4
Temple
You are at the Temple. You can go to the Jungle or to the Clearing.

Path
p0
Beach
Rockface

Path
p1
Beach
Jungle

Path
p2
Jungle
Clearing

Path
p3
Jungle
Temple

Path
p4
Clearing
Temple

Item
it0
Sword
No
An ordinary steel sword.
Beach

Item
it1
Banana
No
An ordinary banana, what'd you expect?
Player

Item
it2
Rope
No
A 20 meter long piece of rope.
Player