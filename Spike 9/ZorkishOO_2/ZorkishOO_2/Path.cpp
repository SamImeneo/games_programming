#include "Path.h"


using namespace std;
Path::Path()
{
}


Path::~Path()
{
}

void Path::setID(string newID)
{
	id = newID;
}

std::string Path::getID()
{
	return id;
}

void Path::setPoints(vector<Location> points)
{
	point1 = points[0];
	point2 = points[1];
}

std::vector<Location> Path::getPoints()
{
	vector<Location> points;
	points.push_back(point1);
	points.push_back(point2);

	return points;
}
