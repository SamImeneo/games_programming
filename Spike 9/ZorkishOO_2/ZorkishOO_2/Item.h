#pragma once
#include <iostream>

class Item
{
public:
	Item();
	~Item();
	virtual void setName(std::string);
	std::string getItemName();
	void setID(std::string);
	std::string getID();
	void setDescription(std::string);
	std::string getDescription();

private:
	std::string lName;
	std::string itemID;
	std::string itemDescription;
};

