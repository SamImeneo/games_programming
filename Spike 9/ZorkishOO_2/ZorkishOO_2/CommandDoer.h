#pragma once
#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include "Location.h"
#include "Path.h"
#include "Player.h"
#include "Banana.h"

class CommandDoer
{
public:
	CommandDoer();
	~CommandDoer();

	void checkForLocation(std::string, Player&, std::vector<Location>&, std::vector<Path>&);
	void lookItem(std::string, Player&);

	void lookAtIn(std::string, std::string, Player&, std::vector<Location>&, std::vector<Path>&);
	void lookForItemInBag(std::string, Player&);

	void lookLocation(std::string, Player&);
	void peel(std::string, Player&);

};

