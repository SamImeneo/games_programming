#pragma once
#include "Item.h"
class Banana :
	public Item
{
public:
	Banana();
	~Banana();

	void peelBanana();
	bool isBananaPeeled();
private:
	bool peeled = false;
};

