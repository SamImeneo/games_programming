#pragma once
#include "Inventory.h"
#include "Location.h"
#include "Bag.h"


class Player
{
public:
	Player();
	~Player();
	void draw();
	void addItem(Item&);
	void removeItem(int);
	int getInventorySize();
	Item getItem(int);

	Bag* getBag();
	
	void setLocation(Location&);
	Location getLocation();

private:
	Bag lBag;
	Location lLocation;
};

