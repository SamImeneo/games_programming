#pragma once
#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include "Location.h"
#include "Path.h"
#include "Player.h"
#include "Item.h"
#include "Bag.h"
#include "Banana.h"

class SetupAdventure
{
public:
	SetupAdventure();
	~SetupAdventure();

	void Setup(std::vector<Location>&, std::vector<Path>&, Player&);
	void createBag(std::string, std::string, std::string, std::string, Player&, std::vector<Location>&);
	void createItem(std::string, std::string, std::string, std::string, Player&, std::vector<Location>&);
	void createBanana(std::string, std::string, std::string, std::string, Player&, std::vector<Location>&);
	void putInLocation(Item&, std::string, Player&, std::vector<Location>&);
};

