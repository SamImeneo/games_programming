#pragma once
#include "Item.h"
#include <iostream>
#include <vector>

class Inventory
{
public:
	Inventory();
	~Inventory();
	void addItem(Item&);
	void removeItem(int);
	void removeItem(std::string s);

	int getInventorySize();
	Item getItem(int);


	std::vector<Item> getInventory();

private:
	std::vector<Item> lInventory;
};

