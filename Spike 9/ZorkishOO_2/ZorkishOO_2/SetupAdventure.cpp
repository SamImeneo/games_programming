#include "SetupAdventure.h"


using namespace std;
SetupAdventure::SetupAdventure()
{
}


SetupAdventure::~SetupAdventure()
{
}

void SetupAdventure::Setup(vector<Location>& locationVector, vector<Path>& pathVector, Player& lPlayer)
{
	// read in text file, create all the locations etc here
	ifstream inFile("AdventureMain.txt");
	string line;

	if (inFile.is_open())
	{
		// while there is still text in the file, read it
		while (!inFile.eof())
		{
			// get a line
			getline(inFile, line);
			// if the line says "location"
			if (line == "Location")
			{
				//set the next line to be the id
				string locID;
				getline(inFile, locID);

				// the one after to be the name
				string locName;
				getline(inFile, locName);

				// and the next to be the desctiption
				string locDesc;
				getline(inFile, locDesc);

				// make a new location, and assign those values to the location
				Location lLocation;
				lLocation.setID(locID);
				lLocation.setName(locName);
				lLocation.setDescription(locDesc);

				// put that location in the vector of locations
				locationVector.push_back(lLocation);
			}

			// if the line says "path"
			if (line == "Path")
			{
				// set the next line to be the id
				string pathID;
				getline(inFile, pathID);

				// the line after that to be the first point of the path
				string point1;
				getline(inFile, point1);

				// and the line after that to be the second point of the path
				string point2;
				getline(inFile, point2);

				// create two locations
				Location p1;
				Location p2;

				// find the locations in the vector that match those locations, and assign them to the new locations
				for (int i = 0; i < locationVector.size(); i++)
				{
					if (locationVector[i].getName() == point1)
					{
						p1 = locationVector[i];
					}
					else if (locationVector[i].getName() == point2)
					{
						p2 = locationVector[i];
					}
				}

				// put those locations into a vector
				vector<Location> tempLocVector;
				tempLocVector.push_back(p1);
				tempLocVector.push_back(p2);
				// make a new path with those values
				Path lPath;
				lPath.setID(pathID);
				lPath.setPoints(tempLocVector);

				// put that path into a vector of paths
				pathVector.push_back(lPath);

			}

			// if the line says "item"
			if (line == "Item")
			{
				// set the next line to be the id
				string itemID;
				getline(inFile, itemID);

				// the line after that to be the name of the item
				string itemName;
				getline(inFile, itemName);

				// and the line after that to determine if the item has an inventory
				string hasInventory;
				getline(inFile, hasInventory);

				// the line after that is the description of th item
				string itemDescription;
				getline(inFile, itemDescription);

				// and the line after that is the items location
				string itemLocation;
				getline(inFile, itemLocation);

				if (hasInventory == "Yes")
				{
					createBag(itemID, itemName, itemDescription, itemLocation, lPlayer, locationVector);
				}
				else if (hasInventory == "No")
				{
					if (itemName == "Banana")
					{
						createBanana(itemID, itemName, itemDescription, itemLocation, lPlayer, locationVector);
					}
					else
					{
						createItem(itemID, itemName, itemDescription, itemLocation, lPlayer, locationVector);
					}
				}	
			}
		}
		inFile.close();
	}
	/*
	for each (Location l in locationVector)
	{
		cout << "location: " << l.getName() << endl;
	}

	for each (Path p in pathVector)
	{
		cout << "path from: " << p.getPoints()[0].getName() << " to " << p.getPoints()[1].getName() << endl;
	}*/
}

void SetupAdventure::createBag(string bagID, string bagName, string bagDescription, string bagLocation, Player& lPlayer, vector<Location>& locationVector)
{
	Bag lBag;

	lBag.setID(bagID);
	lBag.setName(bagName);
	lBag.setDescription(bagDescription);
	putInLocation(lBag, bagLocation, lPlayer, locationVector);
	//////////////////////////////////////////////////////////
	
}

void SetupAdventure::createItem(string itemID, string itemName, string itemDescription, string itemLocation, Player& lPlayer, vector<Location>& locationVector)
{
	Item lItem;

	lItem.setID(itemID);
	lItem.setName(itemName);
	lItem.setDescription(itemDescription);
	putInLocation(lItem, itemLocation, lPlayer, locationVector);
}

void SetupAdventure::createBanana(string itemID, string itemName, string itemDescription, string itemLocation, Player& lPlayer, vector<Location>& locationVector)
{
	Banana lBanana;

	lBanana.setID(itemID);
	lBanana.setName(itemName);
	lBanana.setDescription(itemDescription);
	putInLocation(lBanana, itemLocation, lPlayer, locationVector);
}


void SetupAdventure::putInLocation(Item& lItem, string itemLocation, Player& lPlayer, vector<Location>& locationVector)
{
	if (itemLocation == "Player")
	{
		/*cout << "item: " << lItem.getItemName() << endl;
		cout << "adding item to player inventory" << endl;
		cout << "inventory before: " << lPlayer.getBag()->getInventorySize() << endl;*/
		lPlayer.addItem(lItem);
		/*cout << "inventory after: " << lPlayer.getBag()->getInventorySize() << endl;*/
	}
	else
	{
		for (int i = 0; i < locationVector.size(); i++)
		{
			if (locationVector[i].getName() == itemLocation)
			{
				locationVector[i].addItem(lItem);
				/*cout << "inv size loc" << locationVector[i].getInventorySize() << endl;*/
			}
		}
	}
}
